<?php
/*$txt = "Hello World!";
$number = 10;

echo $txt;
echo $number;

echo "<h4>This is a simple heading .</h4>";
echo "<h4 style='color : Blue;'>This is heading with style.</h4>";

$a = 123;
var_dump($a);
echo "<br>";

$b = -123;
var_dump($b);
echo "<br>";

$c = 0x1A;
var_dump($c);
echo "<br>";

$d = 0123;
var_dump($d);
echo "<br>";

$colors = array("Red", "Green", "Blue");
var_dump($colors);
echo "<br>";

class greeting{
	public $str = "Hello World!";

	//methods
	function show_greeting(){
		return $this->str;
	}
}

$message = new greeting;
var_dump($message);

echo $message->show_greeting();*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Example of PHP GET method</title>
</head>
<body>
<?php
	if(isset($_GET["name"])){
		echo "<p>Hi, ".$_GET["name"]."</p>";
	}
?>
<form method="get" action="<?php echo $_SERVER["PHP_SELF"];?>">
	<label for="inputName">Name:</label>
	<input type="text" name="name" id="inputName">
	<input type="submit" value="Submit">
</form>
</body>
